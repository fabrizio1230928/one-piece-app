import { Component,OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AnimeApiService } from '../../../data/services/api/anime-api.service';

@Component({
  selector: 'app-movies-list',
  templateUrl: './movies-list.component.html',
  styleUrls: ['./movies-list.component.css']
})
export class MoviesListComponent implements OnInit {
  movies: any[] = [];

  constructor(private animeApiService: AnimeApiService, private router: Router) {}

  ngOnInit() {
    this.animeApiService.getOnePieceMovies().subscribe((data: any) => {
      this.movies = data.data;
      console.log('Películas cargadas:', this.movies);
    });
  }
  

  showMovieDetails(movieId: number) {
    // Implementar la navegación a la vista de detalles de la película
    this.router.navigate(['movie-details', movieId]);
  }
}{

}
