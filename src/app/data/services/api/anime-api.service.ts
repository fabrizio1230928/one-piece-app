import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AnimeApiService {
  private baseUrl = 'https://api.jikan.moe/v4'

  constructor(private http: HttpClient) { }

  //Obtenemos la lista de peliculas de "One Pice"
  getOnePieceMovies(): Observable<any> {
    const url = `${this.baseUrl}/anime?q=one%20piece&type=Movie`;
    return this.http.get(url)
      .pipe(
        tap(data => console.log('Datos de películas:', data))
      );
  }
  

  //obtenemos los personajes segun la pelicula (mal_id)
  getCharactersForMovie(movieId: number): Observable<any> {
    const url = `${this.baseUrl}/anime/${movieId}/characters`;
    return this.http.get(url)
  }

  //obtenemos los detalles de un personaje (character_mal_id)
  getCharacterDetails(characterId: number): Observable<any> {
    const url = `${this.baseUrl}/characters/${characterId}/full`;
    return this.http.get(url);
  }
}
